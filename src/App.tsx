import React, { Component, Suspense } from "react";
import "./App.css";
// import 'bootstrap/dist/css/bootstrap.css'
import { PlaylistsView } from "./Playlists/views/PlaylistsView";

import { Route, Switch, Redirect } from "react-router";
import { Link, NavLink } from "react-router-dom";

export const LazySearch = React.lazy(() =>
  import("./Search/views/MusicSearch")
);

class App extends Component {
  render() {
    return (
      <>
        <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
          <div className="container">
            <NavLink className="navbar-brand" to="/">
              Music App
            </NavLink>

            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <NavLink
                    activeClassName="placki active"
                    // isActive={p => console.log(p) as any && false}
                    className="nav-link"
                    to="/playlists"
                  >
                    Playlists
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/search">
                    Search
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <div className="container">
          <div className="row">
            <div className="col">
              <Switch>
                <Redirect path="/" exact={true} to="/playlists" />

                <Route
                  path="/playlists/:id?"
                  exact={true}
                  component={PlaylistsView}
                />

                {/* <Route path="/search" component={MusicSearch} /> */}
                <Route
                  path="/search"
                  render={() => {
                    return (
                      <Suspense fallback={<p>Please wait loading...</p>}>
                        <LazySearch />
                      </Suspense>
                    );
                  }}
                />

                <Route
                  path="*"
                  render={() => <p>404 page not found! ;-( </p>}
                />
              </Switch>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default App;
