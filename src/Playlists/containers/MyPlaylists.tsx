import { connect, MapStateToProps, MapDispatchToProps } from "react-redux"; //  npm i --save @types/react-redux
import { AppState, store } from "../../store";
import { PlaylistsList } from "../components/PlaylistsList";
import { Playlist } from "../../Models/Playlist";
import { selectPlaylist } from "../../Core/playlists.actions";

type StateProps = {
  playlists: Playlist[];
  selected: Playlist | null;
};

type DProps = {
  // onSelect: (p: Playlist) => void;
};

// Mapping Store state to Component Props
const mapStateToProps: MapStateToProps<
  StateProps,
  { selected: number },
  AppState
> = (state, props) => {
  return {
    playlists: state.playlists.items,
    selected:
      state.playlists.items.find(
        // Find selected by Id
        p => props.selected == p.id
      ) || null
  };
};

// Mapping callback props to store.dispatch(action)
const mapDispatchToProps: MapDispatchToProps<DProps, {}> = dispatch => ({
  // onSelect(p: Playlist) {
  //   dispatch(selectPlaylist(p.id));
  // }
});

// Connector - Higher Order Component with Redux Context
const withPlaylists = connect(
  mapStateToProps,
  mapDispatchToProps,
  // (stateProps, dispatchProps: DProps, ownProps) => {
  //   if (ownProps.selected !== stateProps.selected) {
  //     dispatchProps.onSelect(ownProps.selected);
  //   }
  //   return {
  //     ...stateProps,
  //     ...dispatchProps
  //   };
  // }
);

// Connected Component
export const MyPlaylists = withPlaylists(PlaylistsList);

// ====

export const MyPlaylists2 = connect<StateProps, {}, {}, AppState>(
  (state, ownProps = {}) => ({
    playlists: state.playlists.items,
    selected:
      state.playlists.items.find(p => state.playlists.selected == p.id) || null
  }),
  dispatch => ({
    onSelect(p: Playlist) {
      dispatch(selectPlaylist(p.id));
    }
  })
)(PlaylistsList);
