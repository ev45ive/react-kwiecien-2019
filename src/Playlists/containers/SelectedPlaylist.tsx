import { connect } from "react-redux";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { AppState } from "../../store";
import { Playlist } from "../../Models/Playlist";
import React, { useState } from "react";
import { updatePlaylist } from "../../Core/playlists.actions";
import { bindActionCreators } from "redux";

type StateProps = {
  playlist: Playlist | null;
};
type DispatchProps = {
  onSave(p: Playlist): any;
};

const withSelectedPlaylist = connect<StateProps, DispatchProps, {selected:number}, AppState>(
  (state: AppState,props:{selected:number}) => ({
    playlist:
      state.playlists.items.find(p => props.selected == p.id) || null
  }),
  dispatch =>
    bindActionCreators(
      {
        onSave: updatePlaylist
      },
      dispatch
    )
);

const IsPlaylistSelected = (props: StateProps & DispatchProps) => {
  const playlist = props.playlist;
  const [isEditing, setEditing] = useState(false)
  
  return (
    <>
      {playlist !== null ? (
        isEditing ? (
          <PlaylistForm onSave={props.onSave} playlist={playlist} onCancel={()=>setEditing(false)} />
        ) : (
          <PlaylistDetails playlist={playlist} onEdit={()=>setEditing(true)} />
        )
      ) : (
        <p>Please select playlist</p>
      )}
    </>
  );
};

export const SelectedPlaylist = withSelectedPlaylist(IsPlaylistSelected);
