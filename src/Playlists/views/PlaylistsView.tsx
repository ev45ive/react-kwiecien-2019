import React, { FunctionComponent, useEffect } from "react";
import { MyPlaylists } from "../containers/MyPlaylists";
import { SelectedPlaylist } from "../containers/SelectedPlaylist";
import { RouteComponentProps } from "react-router";

type P = {} & RouteComponentProps<{ id: string }>;

export const PlaylistsView: FunctionComponent<P> = ({
  match: {
    params: { id }
  },
  history: { replace, listen }
}) => {
  // useEffect(() => listen(console.log), [1]);

  return (
    <div>
      <div className="row">
        <div className="col">
          <MyPlaylists
            selected={parseInt(id)}
            onSelect={p => replace("/playlists/" + p.id)}
          />
        </div>
        <div className="col">
          <SelectedPlaylist selected={parseInt(id)} />
        </div>
      </div>
    </div>
  );
};
