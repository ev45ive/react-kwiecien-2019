import { Playlist } from "../../Models/Playlist";
import React from "react";
import { Card } from "../../components/Card";

type State = {
  playlist: Playlist;
};

type Props = {
  playlist: Playlist;
  onSave: (playlist: Playlist) => void;
  onCancel: () => void;
};

export class PlaylistForm extends React.PureComponent<Props, State> {
  state: State = {
    playlist: this.props.playlist
  };

  constructor(props: Props) {
    super(props);
    console.log("constructor");
  }

  componentDidMount() {
    // input.focus()
    if (this.inputRef.current) {
      this.inputRef.current.focus()
      // $(this.inputRef.current).plugin()
    }

    console.log("componentDidMount");
  }

  componentWillUnmount() {
    // $(this.inputRef.current).destroy()
    console.log("componentWillUnmount");
  }

  static getDerivedStateFromProps(newProps: Props, newState: State): State {
    console.log("getDerivedStateFromProps", newProps, newState);

    return newProps.playlist.id !== newState.playlist.id
      ? {
          playlist: newProps.playlist
        }
      : newState;
  }

  // shouldComponentUpdate(newProps: Props, newState: State) {
  //   console.log("shouldComponentUpdate");
  //   // return true;
  //   return this.state.playlist !== newState.playlist;
  // }

  getSnapshotBeforeUpdate(prevProps = {}, prevState = {}) {
    console.log("getSnapshotBeforeUpdate");
    return {};
  }

  componentDidUpdate(prevProps = {}, prevState = {}, snapshot: {}) {
    console.log("componentDidUpdate");
  }

  inputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.currentTarget,
      type = target.type,
      fieldName = target.name,
      value = type == "checkbox" ? target.checked : target.value;

    this.setState({
      playlist: {
        ...this.state.playlist,
        [fieldName]: value
      }
    });
  };

  save = () => {
    this.props.onSave(this.state.playlist);
  };

  cancel = () => {
    this.props.onCancel();
  };

  inputRef = React.createRef<HTMLInputElement>();

  render() {
    return (
      <Card>
        <div className="form-group">
          <label>Name:</label>
          <input
            // ref={(elem)=> elem && elem.focus()}
            ref={this.inputRef}
            type="text"
            className="form-control"
            value={this.state.playlist.name}
            name="name"
            onChange={this.inputChange}
          />
        </div>

        <div className="form-group">
          <label>Favorite:</label>
          <input
            type="checkbox"
            checked={this.state.playlist.favorite}
            name="favorite"
            onChange={this.inputChange}
          />
        </div>

        <div className="form-group">
          <label>Color:</label>
          <input
            type="color"
            value={this.state.playlist.color}
            name="color"
            onChange={this.inputChange}
          />
        </div>
        {/* <input type="button" value="Edit" className="btn btn-info"/> */}
        <input
          type="button"
          value="Cancel"
          className="btn btn-danger"
          onClick={this.cancel}
        />
        <input
          type="button"
          value="Save"
          className="btn btn-success"
          onClick={this.save}
        />
      </Card>
    );
  }
}
