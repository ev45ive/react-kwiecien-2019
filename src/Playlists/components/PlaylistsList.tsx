import React from "react";
import { Playlist } from "../../Models/Playlist";

type Props = {
  playlists: Playlist[];
  selected: Playlist | null;
  onSelect: (p: Playlist) => void;
};

export class PlaylistsList extends React.Component<Props, {}> {
  
  select(p: Playlist) {
    this.props.onSelect(p);
  }

  render() {
    return (
      <div>
        <div className="list-group">
          {this.props.playlists.map((playlist, index) => (
            <div
              className={
                "list-group-item" +
                (this.props.selected == playlist ? " active" : "")
              }
              key={playlist.id}
              onClick={() => this.select(playlist)}
            >
              {index + 1}. {playlist.name}
            </div>
          ))}
        </div>
      </div>
    );
  }
}
