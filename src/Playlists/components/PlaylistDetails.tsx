import React, { Fragment, FunctionComponent } from "react";
import { Playlist } from "../../Models/Playlist";
import { Card } from "../../components/Card";

type Props = {
  playlist: Playlist;
  onEdit?: (playlist: Playlist) => void;
};

export const PlaylistDetails: FunctionComponent<Props> = React.memo(({
  playlist,
  onEdit = (p:Playlist) => {}
}) => (
  <Card>
    <dl>
      <dt>Name:</dt>
      <dd>{playlist.name}</dd>
      <dt>Favorite:</dt>
      <dd>{playlist.favorite ? "Yes" : "No"}</dd>
      <dt>Color:</dt>
      <dd
        style={{
          color: playlist.color,
          borderBottom: "1px solid " + playlist.color
        }}
      >
        {playlist.color}
      </dd>
    </dl>
    <input
      type="button"
      value="Edit"
      className="btn btn-info"
      onClick={() => onEdit(playlist)}
    />
  </Card>
));

// PlaylistDetails.defaultProps = {
//   onEdit: (p:Playlist) => {}
// };
