import React from "react";
import { AlbumSearchForm } from "../containers/AlbumSearchForm";
import { AlbumSearchResults } from "../containers/AlbumSearchResults";

type P = {};

export class MusicSearch extends React.Component<P> {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <AlbumSearchForm />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <AlbumSearchResults />
          </div>
        </div>
      </div>
    );
  }
}

export default MusicSearch