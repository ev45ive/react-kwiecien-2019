import { connect } from "react-redux";
import { SearchResults } from "../components/SearchResults";
import { AppState } from "../../store";
import { Album } from "../../Models/Album";

type SProps = {
  results: Album[];
};

export const AlbumSearchResults = connect<SProps, {}, {}, AppState>(state => ({
  results: state.search.results
}))(SearchResults);
