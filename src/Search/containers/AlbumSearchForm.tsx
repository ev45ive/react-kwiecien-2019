import { connect } from "react-redux";
import { SearchForm } from "../components/SearchForm";
import { AppState } from "../../store";
import { withRouter } from "react-router-dom";
import {
  searchStart,
  searchSuccess,
  searchFail,
  searchAlbums
} from "../Search.actions";
import { musicSearch } from "../../services";
import { bindActionCreators, Dispatch, Action } from "redux";

type SProps = {};
type DProps = { onSearch(q: string): void };

export const AlbumSearchForm = withRouter(
  connect<SProps, DProps, {}, AppState>(
    state => ({}),
    (dispatch: any) => ({
      // onSearch: searchAlbums(dispatch)
      onSearch(query: string) {
        dispatch(searchAlbums(query));
      }
      // onSearch(query: string) {
      //   dispatch(searchAlbums(query));
      // }
    })
  )(SearchForm)
);
