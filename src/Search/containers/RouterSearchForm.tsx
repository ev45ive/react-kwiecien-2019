import { MusicSearchContext } from "../MusicSearchProvider";
import { SearchForm } from "../components/SearchForm";
import { withRouter, RouteComponentProps } from "react-router";
import React from "react";


export const RouterSearchForm = withRouter(SearchForm);

// export const MusicSearchForm = (props: RouteComponentProps<{}>) => {
//   const p = new URLSearchParams(props.location.search);
//   const query = p.get("query");

//   return (
//     <MusicSearchContext.Consumer>
//       {({ search }) => {
//         if (query) {
//           search(query);
//         }
//         return <SearchForm onSearch={search} />;
//       }}
//     </MusicSearchContext.Consumer>
//   );
// };