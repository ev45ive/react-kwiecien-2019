import React, { FunctionComponent } from "react";
import { Album } from "../../Models/Album";

type P = {
  album: Album;
} & React.HTMLAttributes<HTMLDivElement>;

export const AlbumCard: FunctionComponent<P> = props => (
  <div {...props} className={'card '+props.className} >
    <img src={props.album.images[0].url} className="card-img-top" />
    <div className="card-body">
      <h5 className="card-title">{props.album.name}</h5>
    </div>
  </div>
);
