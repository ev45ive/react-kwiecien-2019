import React, { Component, PureComponent } from "react";
import { RouteComponentProps, StaticContext } from "react-router";
import { npost } from "q";

// ccsf
// https://getbootstrap.com/docs/4.3/components/input-group/#button-addons

type P = {
  onSearch(query: string): void;
} & RouteComponentProps<any, StaticContext, any>;

type S = {
  query: string;
};

export class SearchForm extends PureComponent<P, S> {
  state = {
    query: ""
  };

  inputRef = React.createRef<HTMLInputElement>();

  search = () => {
    if (this.inputRef.current) {
      const query = this.inputRef.current.value;

      this.props.history.replace({
        // this.props.history.push({
        pathname: "/search",
        search: "?query=" + query
      });
    }
  };

  timeout!: NodeJS.Timeout;

  debouncedSearch = () => {
    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      this.search();
    }, 400);
  };

  // static getDerivedStateFromProps(newProps:P, newState:S){
  //   const p = new URLSearchParams(newProps.location.search);
  //   console.log(p.get('query'),newState.query)
  //   return {
  //     query: p.get("query") || ""
  //   }
  // }

  componentDidMount() {
    const query = this.getQuery();
    if (query != "") {
      this.props.onSearch(query);
    }
  }

  componentDidUpdate() {
    const p = new URLSearchParams(this.props.location.search);
    const query = p.get("query")!;
    if (query && query != this.state.query) {
      this.props.onSearch(query);

      this.setState({ query });
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  getQuery = () => {
    const p = new URLSearchParams(this.props.location.search);
    return p.get("query") || "";
  };

  render() {
    return (
      <div className="input-group mb-3">
        <input
          type="text"
          className="form-control"
          placeholder="Search"
          defaultValue={this.getQuery()}
          onChange={this.debouncedSearch}
          onKeyUp={e => e.keyCode == 13 && this.search()}
          ref={this.inputRef}
        />

        <div className="input-group-append">
          <button className="btn btn-outline-secondary" onClick={this.search}>
            Search
          </button>
        </div>
      </div>
    );
  }
}
