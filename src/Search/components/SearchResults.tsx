import React, { FunctionComponent, Fragment } from "react";
import { AlbumCard } from "./AlbumCard";
import { Album } from "../../Models/Album";
// https://getbootstrap.com/docs/4.3/components/card/#card-groups

import styles from "./SearchResults.module.css";

type P = {
  results: Album[];
};

export const SearchResults: FunctionComponent<P> = ({ results }) => (
  <div className="card-group">
    {results.map((result, index) => (
      <Fragment key={result.id}>
        <AlbumCard album={result} className={styles.cardWidth} />
        {/* === */}
        {(index + 1) % 4 == 0 ? <Spacer /> : null}
      </Fragment>
    ))}
  </div>
);

export const Spacer = () => (
  <div
    style={{
      flex: "1 1 100%"
    }}
  />
);
