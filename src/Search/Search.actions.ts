import { Album } from "../Models/Album";
import { ActionCreator, Action, Dispatch } from "redux";
import { musicSearch } from "../services";

// https://github.com/redux-utilities/redux-actions

export enum SearchActionType {
  SearchStarted = "[Search] SearchStarted",
  SearchSucceded = "[Search] SearchSucceded",
  SearchFailed = "[Search] SearchFailed"
}

export interface SearchState {
  results: Album[];
  query: string;
  pagination: {};
  error: string;
  loading: boolean;
}

interface SearchStarted extends Action<SearchActionType.SearchStarted> {
  payload: { query: string };
}
interface SearchSucceded extends Action<SearchActionType.SearchSucceded> {
  payload: { results: Album[] };
}
interface SearchFailed extends Action<SearchActionType.SearchFailed> {
  payload: { error: string };
}
export type SearchActions = SearchStarted | SearchSucceded | SearchFailed;

export const searchStart = (payload: { query: string }): SearchStarted => ({
  type: SearchActionType.SearchStarted,
  payload
});

export const searchSuccess = (
  payload: SearchSucceded["payload"]
): SearchSucceded => ({
  type: SearchActionType.SearchSucceded,
  payload
});

export const searchFail = (payload: SearchFailed["payload"]): SearchFailed => ({
  type: SearchActionType.SearchFailed,
  payload
});

// export class SelectPlaylist {
//   readonly type = "[Playlist] Select";
//   constructor(readonly payload: { id: string }) {}
// }

// const select = new SelectPlaylist({ id: "123" });

// switch(action.type){
//   case SelectPlaylist.prototype.type: return {

//   }
// };

export const searchAlbums = (query: string) => (
  dispatch: Dispatch<Action<any>>
) => {
  // https://github.com/pburtchaell/redux-promise-middleware/blob/HEAD/docs/introduction.md
  // dispatch({type:"SEARCH", payload: musicSearch.searchAlbums(query)});

  dispatch(searchStart({ query }));
  musicSearch
    .searchAlbums(query)
    .then(results =>
      dispatch(
        searchSuccess({
          results
        })
      )
    )
    .catch(error =>
      dispatch(
        searchFail({
          error
        })
      )
    );
};
