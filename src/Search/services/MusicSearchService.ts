import axios, { AxiosError } from "axios";
import { AlbumsResponse } from "../../Models/Album";
import { auth } from "../../services";

axios.interceptors.request.use(req => {
  req.headers["Authorization"] = `Bearer ${auth.getToken()}`;
  return req;
});

// axios.interceptors.response.use(
//   res => {
//     console.log(res);
//     return res;
//   },
//   res => {
//     console.log(res.response.data.error);
//     return res;
//   }
// );

export class MusicSearchService {
  api_url = "https://api.spotify.com/v1/search";

  searchAlbums(query: string = "batman") {
    return axios
      .get<AlbumsResponse>(this.api_url, {
        params: {
          type: "album",
          q: query
        }
        // headers: {
        //   Authorization: `Bearer ${auth.getToken()}`
        // }
      })
      .then(resp => resp.data.albums.items)
      .catch((error: AxiosError) => {
        // Handle errors
        if (error.response && error.response.status == 401) {
          auth.authorize();
        }
        // throw Error(error.message)
        return Promise.reject(Error(error.message));
      });
  }
}
