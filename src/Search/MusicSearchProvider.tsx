import React from "react";
import { Album } from "../Models/Album";
import { musicSearch } from "../services";

export const MusicSearchContext = React.createContext({
  results: [] as Album[],
  search(query: string) {}
});

// export const connectMusic = function connectMusic<T>(
//   Component: React.ComponentType
// ) {
//   return (props: T) => (
//     <MusicSearchContext.Consumer>
//       {({search, results}) => <Component {...props} />}
//     </MusicSearchContext.Consumer>
//   );
// };

type S = {
  results: Album[];
  error: string;
  loading: boolean;
};

export class MusicSearchProvider extends React.Component<{}, S> {
  state = {
    results: [],
    error: "",
    loading: false
  };

  search = (query: string) => {
    this.setState({
      results: [],
      loading: true,
      error: ""
    });
    musicSearch
      .searchAlbums(query)
      .then(results =>
        this.setState({
          results,
          error: ""
        })
      )
      .catch(err =>
        this.setState({
          error: err.message
        })
      )
      .finally(() =>
        this.setState({
          loading: false
        })
      );
  };

  render() {
    return (
      <MusicSearchContext.Provider
        value={{
          results: this.state.results,
          search: this.search
        }}
      >
        {this.props.children}
      </MusicSearchContext.Provider>
    );
  }
}
