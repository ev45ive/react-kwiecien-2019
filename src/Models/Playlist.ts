// export class Playlist {}

export interface Playlist {
  id: number;
  name: string;
  favorite: boolean;
  color: string;
  /**
   * Tracks Doc
   */
  tracks?: Track[];

  // tracks: Array<Track>
}

interface Track {
  id: number;
  name: string;
}
