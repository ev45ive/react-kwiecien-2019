// https://developer.spotify.com/documentation/web-api/reference/object-model/#album-object-simplified
// https://swagger.io/tools/swagger-ui/
// http://www.jsontots.com/

export interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  artists: Artist[];
  images: AlbumImage[];
}

export interface Artist extends Entity {
  popularity: number;
}

export interface AlbumImage {
  url: string;
  width: number;
  height: number;
}

export interface PagingObject<T> {
  items: T[];
}

export interface ArtistsResponse {
  artists: PagingObject<Artist>;
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
