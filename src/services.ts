import { MusicSearchService } from "./Search/services/MusicSearchService";
import { AuthService } from "./Core/services/AuthService";

export const musicSearch = new MusicSearchService();

export const auth = new AuthService(
  "https://accounts.spotify.com/authorize",
  "70599ee5812a4a16abd861625a38f5a6",
  "token",
  "http://localhost:3000/"
);

// Check if logged in?
auth.getToken()


//  https://jsfiddle.net/ g8t2wc0k/