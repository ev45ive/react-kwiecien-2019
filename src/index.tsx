import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { MusicSearchProvider } from "./Search/MusicSearchProvider";
import { store } from "./store";
import { increment, decrement } from "./Core/actions";
import { Provider } from "react-redux";

// import { HashRouter as Router } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";

(window as any).store = store;
(window as any).inc = increment;
(window as any).dec = decrement;

ReactDOM.render(
  <Router>
    <Provider store={store}>
      <MusicSearchProvider>
        <App />
      </MusicSearchProvider>
    </Provider>
  </Router>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
