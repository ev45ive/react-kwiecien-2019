import { ActionCreator, Action } from "redux";

enum CounterActionTypes {
  Increment = "[Counter] Increment",
  Decrement = "[Counter] Decrement"
}

interface Increment extends Action<"[Counter] Increment"> {
  payload: number;
}

interface Decrement extends Action<"[Counter] Decrement"> {
  payload: number;
}

export type CounterActions = Increment | Decrement;

export const increment: ActionCreator<Increment> = (payload = 1) => ({
  type: "[Counter] Increment",
  payload
});

export const decrement: ActionCreator<Decrement> = (payload = 1) => ({
  type: "[Counter] Decrement",
  payload
});
