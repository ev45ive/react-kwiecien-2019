export class AuthService {
  constructor(
    private auth_url: string,
    private client_id: string,
    private response_type: string,
    private redirect_uri: string
  ) {}

  private token: string | null = null;

  authorize() {
    const url =
      `${this.auth_url}?client_id=${this.client_id}` +
      `&response_type=${this.response_type}` +
      `&redirect_uri=${this.redirect_uri}`;
      
    sessionStorage.removeItem('token')
    location.href = url;
  }

  getToken() {
    const token = sessionStorage.getItem("token");
    if (token) {
      this.token = JSON.parse(token);
    }

    if (location.hash && !this.token) {
      const match = location.hash.match(/#access_token=([^&]+)/);
      this.token = match && match[1];
      sessionStorage.setItem("token", JSON.stringify(this.token));
      location.hash = "";
    }

    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
