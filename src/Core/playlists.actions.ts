import { Action, ActionCreator, ActionCreatorsMapObject } from "redux";
import { Playlist } from "../Models/Playlist";

export type PlaylistsState = {};

export enum ActionTypes {
  Load = "[Playlists] Load",
  Update = "[Playlists] Update",
  Delete = "[Playlists] Delete",
  Select = "[Playlists] Select"
}

interface Load extends Action<ActionTypes.Load> {
  payload: Playlist[];
}
interface Update extends Action<ActionTypes.Update> {
  payload: Playlist;
}
interface Delete extends Action<ActionTypes.Delete> {
  payload: Playlist["id"];
}
interface Select extends Action<ActionTypes.Select> {
  payload: Playlist["id"];
}

export type PlaylistsActions = Load | Update | Delete | Select;


export const loadPlaylist: ActionCreator<Load> = (payload: Playlist[]) => ({
  type: ActionTypes.Load,
  payload
});

(window as any).loadPlaylist = loadPlaylist

export const updatePlaylist: ActionCreator<Update> = (payload: Playlist) => ({
  type: ActionTypes.Update,
  payload
});

export const selectPlaylist: ActionCreator<Select> = (
  payload: Playlist["id"]
) => ({
  type: ActionTypes.Select,
  payload
});

export const deletePlaylist: ActionCreator<Delete> = (
  payload: Playlist["id"]
) => ({
  type: ActionTypes.Delete,
  payload
});

// const m: ActionCreatorsMapObject<Load | Update | Delete | Select> = {
//   Load: () => ({ type: ActionTypes.Load }),
//   Update: () => ({ type: ActionTypes.Update }),
//   Select: () => ({ type: ActionTypes.Select }),
//   Delete: () => ({ type: ActionTypes.Delete })
// };
