import { CounterActions } from "../Core/actions";
import { Reducer, AnyAction } from "redux";

export const counter: Reducer<number, AnyAction> = (state = 0, action) => {
  
  switch (action.type) {
    case "[Counter] Increment":
      return state + action.payload;
    case "[Counter] Decrement":
      return state - action.payload;
    default:
      return state;
  }
};
