import { Reducer, Action, AnyAction } from "redux";
import {
  SearchState,
  SearchActionType,
  SearchActions
} from "../Search/Search.actions";

export const search: Reducer<SearchState, SearchActions> = (
  state = {
    error: "",
    loading: false,
    pagination: {},
    query: "",
    results: []
  },
  action
) => {
  switch (action.type) {
    case SearchActionType.SearchStarted:
      return {
        ...state,
        loading: true,
        error: "",
        results: [],
        query: action.payload.query
      };

    case SearchActionType.SearchSucceded:
      return {
        ...state,
        loading: false,
        results: action.payload.results
      };

    case SearchActionType.SearchFailed:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };

    default:
      return state;
  }
};
