import { AnyAction, Action } from "redux";
import { PlaylistsActions, ActionTypes } from "../Core/playlists.actions";
import { Playlist } from "../Models/Playlist";

const data: Playlist[] = [
  {
    id: 123,
    name: "React Hits!",
    favorite: true,
    color: "#ff00ff"
  },
  {
    id: 234,
    name: "React TOP 20!",
    favorite: false,
    color: "#ffff00"
  },
  {
    id: 345,
    name: "Best of React!",
    favorite: true,
    color: "#00ffff"
  }
];

export type PlaylistsState = {
  items: Playlist[];
  selected?: Playlist["id"];
};

const initialState: PlaylistsState = {
  items: data as Playlist[]
  // selected
};

export const playlists = (state = initialState, action: PlaylistsActions) => {
  switch (action.type) {
    case ActionTypes.Load:
      return {
        ...state,
        items: action.payload
      };

    case ActionTypes.Select:
      return {
        ...state,
        selected: action.payload
      };

    case ActionTypes.Update:
      return {
        ...state,
        selected: action.payload.id,
        items: state.items.map(p =>
          p.id == action.payload.id ? action.payload : p
        )
      };

    default:
      return state;
  }
};
