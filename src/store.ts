import {
  createStore,
  combineReducers,
  applyMiddleware,
  Middleware
} from "redux";
import { counter } from "./Reducers/CounterReducer";
import { playlists, PlaylistsState } from "./Reducers/PlaylistsReducer";
import { SearchState } from "./Search/Search.actions";
import { search } from "./Reducers/search.reducer";

export interface AppState {
  counter: number;
  playlists: PlaylistsState;
  search: SearchState;
}

export const rootReducer = combineReducers({
  counter,
  playlists,
  search
});

const loggerMiddleware: Middleware = store => next => action => {
  console.log(action);
  const state = next(action);
  console.log(state);
  return state;
};

import thunk from "redux-thunk";
// const asyncMiddleware: Middleware = store => next => action => {
//   if ("function" == typeof action) {
//     action(store.dispatch, store.getState);
//   } else {
//     return next(action);
//   }
// };



import { composeWithDevTools } from 'redux-devtools-extension';

export const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk, loggerMiddleware))
);

// export const rootReducer: Reducer<AppState> = (
//   state = {
//     counter: 0
//   },
//   action: AnyAction
// ) => {
//   return {
//     ...state,
//     counter: counter(state.counter, action as CounterActions)
//   };
// };
