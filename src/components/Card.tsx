import React, { FunctionComponent } from "react";

export const Card: FunctionComponent = ({ children }) => {
  return (
    <div className="card">
      <div className="card-body">{children}</div>
    </div>
  );
};
