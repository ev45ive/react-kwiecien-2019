```ts
inc = (payload = 1) => ({ type: "INC", payload });
dec = (payload = 1) => ({ type: "DEC", payload });
addTodo = payload => ({ type: "TODO_ADD", payload });

[inc(), inc(3), addTodo("Placki!"), addTodo("123"), inc(), dec(2)].reduce(
  (state, action) => {
    switch (action.type) {
      case "INC":
        return { ...state, counter: state.counter + action.payload };
      case "DEC":
        return { ...state, counter: state.counter - action.payload };
      case "TODO_ADD":
        return { ...state, todos: [...state.todos, action.payload] };
      default:
        return sum;
    }
  },
  {
    counter: 0,
    todos: []
  }
);
```
